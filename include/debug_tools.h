
#ifndef RAY_TRACER_HEADER_DEBUG_TOOLS_H
#define RAY_TRACER_HEADER_DEBUG_TOOLS_H

#define $crash() *((i32 *)0) = 0

#if defined(DEBUG_MODE)

    #define $assert(expression) if (!(expression)) $crash()

#else

    #define $assert(expression) do {} while (0)

#endif

#endif