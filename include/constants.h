
#ifndef RAY_TRACER_HEADER_CONSTANTS_H
#define RAY_TRACER_HEADER_CONSTANTS_H

#include "common.h"


const f64 pi = 3.14159265358979323846;

const f64 radians_to_degrees = 180.0 / pi;
const f64 degrees_to_radians = pi / 180.0;

const f64 k_epsilon = 0.001;


#endif