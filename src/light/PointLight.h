
#ifndef RAY_TRACER_HEADER_POINT_LIGHT_H
#define RAY_TRACER_HEADER_POINT_LIGHT_H

#include "common.h"

#include "math/Point.h"
#include "shading/Color.h"


class PointLight
{

    public:
        const Point position;
        const f64 power;
        const Color color;

    public:
        PointLight(f64 x, f64 y, f64 z, f64 power, const Color &color);

        PointLight(const PointLight &other) = default;
        PointLight(PointLight &&) = default;
        PointLight &operator =(const PointLight &other) = default;
        PointLight &operator =(PointLight &&) = default;
        ~PointLight() = default;
        
        PointLight() = delete;

    public:
        f64 get_L(const Point &hit_point) const;

};


#endif