
#include "PointLight.h"

#include "constants.h"


PointLight::PointLight(f64 x, f64 y, f64 z, f64 power, const Color &color)
    : position(x, y, z), power(power), color(color)
{
    $assert(power > 0);
}


f64 PointLight::get_L(const Point &hit_point) const
{
    return power / (4 * pi * (position.distance_to_squared(hit_point)));
}
