
#ifndef RAY_TRACER_HEADER_MATERIAL_H
#define RAY_TRACER_HEADER_MATERIAL_H

#include "common.h"

#include "Color.h"


struct Material
{
    const f64 kd;
    const Color color;
};


#endif