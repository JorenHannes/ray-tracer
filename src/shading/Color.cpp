
#include "Color.h"

#include <math.h>



Color::Color()
    : red(0), green(0), blue(0)
{

}

Color::Color(f64 red, f64 green, f64 blue)
    : red(red), green(green), blue(blue)
{
    $assert(red >= 0);
    $assert(green >= 0);
    $assert(blue >= 0);
}


Color Color::clamp() const
{
    return Color(
        (red < 0 ? 0 : (red > 1 ? 1 : red)),
        (green < 0 ? 0 : (green > 1 ? 1 : green)),
        (blue < 0 ? 0 : (blue > 1 ? 1 : blue))
    );
}

Color Color::normalize() const
{
    f64 max_component = (red > green ? red : (green > blue ? green : blue));
    if (max_component <= 1)
        return *this;

    return Color(
        red / max_component,
        green / max_component,
        blue / max_component
    );
}

Color Color::pow(f64 exponent) const
{
    return Color(
        std::pow(red, exponent),
        std::pow(green, exponent),
        std::pow(blue, exponent)
    );
}


bool Color::operator ==(const Color &other) const
{
    return (this->red == other.red) && (this->green == other.green) && (this->blue == other.blue);
}

bool Color::operator !=(const Color &other) const
{
    return (this->red != other.red) || (this->green != other.green) || (this->blue != other.blue);
}

Color Color::operator +(const Color &other) const
{
    return Color(
        this->red + other.red,
        this->green + other.green,
        this->blue + other.blue
    );
}

Color Color::operator -(const Color &other) const
{
    return Color(
        this->red - other.red,
        this->green - other.green,
        this->blue - other.blue
    );
}

Color Color::operator *(const Color &other) const
{
    return Color(
        this->red * other.red,
        this->green * other.green,
        this->blue * other.blue
    );
}

Color Color::operator *(f64 scalar) const
{
    return Color(
        this->red * scalar,
        this->green * scalar,
        this->blue * scalar
    );
}

Color Color::operator /(f64 divisor) const
{
    $assert(divisor != 0);

    return Color(
        this->red / divisor,
        this->green / divisor,
        this->blue / divisor
    );
}
