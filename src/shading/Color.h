
#ifndef RAY_TRACER_HEADER_COLOR_H
#define RAY_TRACER_HEADER_COLOR_H

#include "common.h"


class Color
{

    public:
        f64 red;
        f64 green;
        f64 blue;

    public:
        Color();
        Color(f64 red, f64 green, f64 blue);
        
        Color(const Color &other) = default;
        Color(Color &&) = default;
        ~Color() = default;
        Color &operator =(const Color &other) = default;
        Color &operator =(Color &&) = default;

    public:
        Color clamp() const;
        Color normalize() const;

        Color pow(f64 exponent) const;

    public:
        bool operator ==(const Color &other) const;
        bool operator !=(const Color &other) const;

        Color operator +(const Color &other) const;
        Color operator -(const Color &other) const;
        Color operator *(const Color &other) const;
        Color operator *(f64 scalar) const;
        Color operator /(f64 divisor) const;

};


#endif