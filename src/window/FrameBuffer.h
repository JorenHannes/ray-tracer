
#ifndef RAY_TRACER_HEADER_FRAMEBUFFER_H
#define RAY_TRACER_HEADER_FRAMEBUFFER_H

#include "common.h"
#include "Pixel.h"


class FrameBuffer
{
    friend i32 main(i32 argc, char **argv);

    public:
        const i32 width;
        const i32 height;
    private:
        Pixel *buffer;

    public:
        FrameBuffer(i32 width, i32 height);
        ~FrameBuffer();
        
        FrameBuffer() = delete;
        FrameBuffer(const FrameBuffer &other) = delete;
        FrameBuffer &operator =(const FrameBuffer &other) = delete;

    public:
        Pixel &get_pixel(i32 x, i32 y);

        void save_as_image(const char *file_name);

};


#endif