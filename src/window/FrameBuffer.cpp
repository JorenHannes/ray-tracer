
#include "FrameBuffer.h"

#include <stdio.h>



FrameBuffer::FrameBuffer(i32 width, i32 height)
    : width(width), height(height), buffer(new Pixel[width * height])
{
    $assert(width > 0);
    $assert(height > 0);
}

FrameBuffer::~FrameBuffer()
{
    delete[] buffer;
}


Pixel &FrameBuffer::get_pixel(i32 x, i32 y)
{
    $assert(x >= 0);
    $assert(x < width);
    $assert(y >= 0);
    $assert(y < height);

    return buffer[(y * width) + x];
}