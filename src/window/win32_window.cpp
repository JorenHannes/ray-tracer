
#include "common.h"
#include "FrameBuffer.h"

#include <windows.h>
#include <stdio.h>



bool running;

#define window_width 1280
#define window_height 720


void init_scene(FrameBuffer &buffer);

LRESULT window_callback(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;

    switch (message)
    {
        case WM_CLOSE:
        case WM_DESTROY:
        {
            running = false;
        } break;

        default:
        {
            result = DefWindowProc(window, message, wParam, lParam);
        } break;
    }

    return result;
}

internal DWORD thread_func(LPVOID lpParam)
{
    init_scene(*((FrameBuffer *)lpParam));
}


int main(int argc, char *argv[])
{
    HINSTANCE instance = GetModuleHandle(NULL);

    WNDCLASS window_class = {};
    window_class.style = CS_HREDRAW | CS_VREDRAW;
    window_class.lpfnWndProc = window_callback;
    window_class.hInstance = instance;
    window_class.lpszClassName = "ray_tracer_window_class";

    if (RegisterClass(&window_class))
    {

        HWND window_handle = CreateWindowEx(
                NULL, // DWORD     dwExStyle,
                window_class.lpszClassName, // LPCTSTR   lpClassName,
                "Ray Tracer", // LPCTSTR   lpWindowName,
                WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | WS_VISIBLE, // DWORD     dwStyle,
                CW_USEDEFAULT, // int       x,
                CW_USEDEFAULT, // int       y,
                window_width, // int       nWidth,
                window_height, // int       nHeight,
                NULL, // HWND      hWndParent,
                NULL, // HMENU     hMenu,
                instance, // HINSTANCE hInstance,
                NULL // LPVOID    lpParam
        );

        if (window_handle)
        {
            
            FrameBuffer buffer(window_width, window_height);
            for (u32 y = 0; y < buffer.height; y++)
            for (u32 x = 0; x < buffer.width; x++)
            {
                buffer.get_pixel(x, y).red = 0;
                buffer.get_pixel(x, y).green = 0;
                buffer.get_pixel(x, y).blue = 0;
            }


            BITMAPINFO info = {};
            info.bmiHeader.biSize = sizeof(info.bmiHeader);
            info.bmiHeader.biWidth = buffer.width;
            info.bmiHeader.biHeight = buffer.height;
            info.bmiHeader.biPlanes = 1;
            info.bmiHeader.biBitCount = sizeof(Pixel) * 8;
            info.bmiHeader.biCompression = BI_RGB;

            HANDLE thread = CreateThread( 
                NULL,                   // default security attributes
                0,                      // use default stack size  
                thread_func,       // thread function name
                &buffer,          // argument to thread function 
                0,                      // use default creation flags 
                0);            // returns the thread identifier


            running = true;
            while (true)
            {

                MSG message = {};
                while (PeekMessage(&message, NULL, NULL, NULL, PM_REMOVE))
                {
                    if (message.message == WM_QUIT)
                        running = false;

                        TranslateMessage(&message);
                        DispatchMessage(&message);
                }

                HDC device_context = GetDC(window_handle);
                StretchDIBits(
                    device_context,
                    0, 0, buffer.width, buffer.height,
                    0, 0, buffer.width, buffer.height,
                    buffer.buffer, &info,
                    DIB_RGB_COLORS, SRCCOPY);
                ReleaseDC(window_handle, device_context);

                if (!running)
                    break;

            }

            $assert(!running);

            TerminateThread(thread, ERROR_SUCCESS);
            CloseHandle(thread);

        }
        else
        {
            printf("Error: Window creation failed!");
        }

    }
    else
    {
        printf("Error: Window Class creation failed!");
    }

    return 0;
}