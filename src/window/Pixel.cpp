
#include "Pixel.h"



Pixel &Pixel::operator =(const Color &color)
{
    this->red = color.red * 255;
    this->green = color.green * 255;
    this->blue = color.blue * 255;
    return *this;
}
