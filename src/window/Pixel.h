
#ifndef RAY_TRACER_HEADER_PIXEL_H
#define RAY_TRACER_HEADER_PIXEL_H

#include "common.h"

#include "shading/Color.h"


class Pixel
{

    public:
        u8 blue;
        u8 green;
        u8 red;
    
    private:
        u8 padding;

    public:
        Pixel() = default;
        ~Pixel() = default;
        
        Pixel(const Pixel &other) = delete;
        Pixel &operator =(const Pixel &other) = delete;

    public:
        Pixel &operator =(const Color &color);

};


#endif