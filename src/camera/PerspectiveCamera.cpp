
#include "PerspectiveCamera.h"

#include "constants.h"
#include "math/Vector.h"
#include <math.h>



PerspectiveCamera::PerspectiveCamera(f64 x, f64 y, f64 z, i32 width, i32 height, f64 fov)
    : origin(x, y, z), d(width / (2 * tan(fov * degrees_to_radians))), width(width), height(height)
{
    $assert(width > 0);
    $assert(height > 0);
    $assert(fov > 0);
}


Ray PerspectiveCamera::generate_ray(const Sample &sample) const
{
    $assert(sample.x >= 0);
    $assert(sample.x < width);
    $assert(sample.y >= 0);
    $assert(sample.y < height);

    return Ray(
        origin,
        (Point(sample.x + 0.5 - (width / 2) + origin.x, sample.y + 0.5 - (height / 2) + origin.y, origin.z - d) - origin).normalize()
    );
}
