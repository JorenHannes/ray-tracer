
#ifndef RAY_TRACER_HEADER_PERSPECTIVE_CAMERA_H
#define RAY_TRACER_HEADER_PERSPECTIVE_CAMERA_H

#include "common.h"

#include "sample/Sample.h"
#include "math/Point.h"
#include "ray/Ray.h"


class PerspectiveCamera
{

    public:
        f64 d;
        i32 width;
        i32 height;
        Point origin;

    public:
        PerspectiveCamera(f64 x, f64 y, f64 z, i32 width, i32 height, f64 fov);
        PerspectiveCamera() = default;
        PerspectiveCamera &operator =(const PerspectiveCamera &other) = default;
        PerspectiveCamera &operator =(PerspectiveCamera &&) = default;
        ~PerspectiveCamera() = default;
        
        PerspectiveCamera(const PerspectiveCamera &other) = delete;
        PerspectiveCamera(PerspectiveCamera &&) = delete;

    public:
        Ray generate_ray(const Sample &sample) const;

};


#endif