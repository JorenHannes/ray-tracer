
#ifndef RAY_TRACER_HEADER_SAMPLER_H
#define RAY_TRACER_HEADER_SAMPLER_H

#include "common.h"

#include "Sample.h"


void sample_uniform(i32 origin_x, i32 origin_y, u32 amount, Sample *samples);

void sample_random(i32 origin_x, i32 origin_y, u32 amount, Sample *samples);

void sample_jittered(i32 origin_x, i32 origin_y, u32 amount, Sample *samples);



#endif