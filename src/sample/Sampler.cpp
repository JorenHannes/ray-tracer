
#include "Sampler.h"

#include <math.h>
#include <stdlib.h>



internal f64 random()
{
    f64 result = ((f64)rand()) / (RAND_MAX + 1);

    $assert(result >= 0);
    $assert(result < 1);
    return result;
}

void sample_uniform(i32 origin_x, i32 origin_y, u32 amount, Sample *samples)
{
    $assert(amount != 0);
    $assert(samples != 0);

    if (amount == 1)
    {
        samples[0] = sample(origin_x + 0.5, origin_y + 0.5);
        return;
    }

    f64 sqrt_amount = sqrt(amount);

    i32 i = 0;
    for (i32 y = 0; y < sqrt_amount; y++)
    for (i32 x = 0; x < sqrt_amount; x++)
    {
        samples[i++] = sample(origin_x + ((x + 0.5) / sqrt_amount), origin_y + ((y + 0.5) / sqrt_amount));
    }
}

void sample_random(i32 origin_x, i32 origin_y, u32 amount, Sample *samples)
{
    $assert(amount != 0);
    $assert(samples != 0);

    for (i32 i = 0; i < amount; i++)
        samples[i] = sample(origin_x + random(), origin_y + random());
}

void sample_jittered(i32 origin_x, i32 origin_y, u32 amount, Sample *samples)
{
    $assert(amount != 0);
    $assert(samples != 0);

    if (amount == 1)
    {
        sample_random(origin_x, origin_y, 1, samples);
        return;
    }

    f64 sqrt_amount = sqrt(amount);

    i32 i = 0;
    for (i32 y = 0; y < sqrt_amount; y++)
    for (i32 x = 0; x < sqrt_amount; x++)
    {
        samples[i++] = sample(origin_x + ((x + random()) / sqrt_amount), origin_y + ((y + random()) / sqrt_amount));
    }
}
