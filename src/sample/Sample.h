
#ifndef RAY_TRACER_HEADER_SAMPLE_H
#define RAY_TRACER_HEADER_SAMPLE_H


struct Sample
{
    f64 x;
    f64 y;
};

inline Sample sample(f64 x, f64 y)
{
    return { x, y };
}


#endif