
#include "Matrix.h"

#include "constants.h"
#include <math.h>



Matrix::Matrix()
{
    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
        values[y][x] = 0;
}


Matrix Matrix::identity()
{
    Matrix result;

    for (i32 i = 0; i < 4; i++)
        result.values[i][i] = 1;

    return result;
}

Matrix Matrix::translation(f64 x, f64 y, f64 z)
{
    Matrix result = Matrix::identity();;

    result.values[0][3] = x;
    result.values[1][3] = y;
    result.values[2][3] = z;

    return result;
}

Matrix Matrix::inverse_translation(f64 x, f64 y, f64 z)
{
    Matrix result = Matrix::identity();;

    result.values[0][3] = -x;
    result.values[1][3] = -y;
    result.values[2][3] = -z;

    return result;
}

Matrix Matrix::rotation_x(f64 angle)
{
    Matrix result;

    f64 sin_v = sin(angle * degrees_to_radians);
    f64 cos_v = cos(angle * degrees_to_radians);

    result.values[0][0] = 1;
    result.values[1][1] = cos_v;
    result.values[1][2] = -sin_v;
    result.values[2][1] = sin_v;
    result.values[2][2] = cos_v;
    result.values[3][3] = 1;

    return result;
}

Matrix Matrix::inverse_rotation_x(f64 angle)
{
    Matrix result;

    f64 sin_v = sin(angle * degrees_to_radians);
    f64 cos_v = cos(angle * degrees_to_radians);

    result.values[0][0] = 1;
    result.values[1][1] = cos_v;
    result.values[1][2] = sin_v;
    result.values[2][1] = -sin_v;
    result.values[2][2] = cos_v;
    result.values[3][3] = 1;

    return result;
}

Matrix Matrix::rotation_y(f64 angle)
{
    Matrix result;

    f64 sin_v = sin(angle * degrees_to_radians);
    f64 cos_v = cos(angle * degrees_to_radians);

    result.values[0][0] = cos_v;
    result.values[0][2] = sin_v;
    result.values[1][1] = 1;
    result.values[2][0] = -sin_v;
    result.values[2][2] = cos_v;
    result.values[3][3] = 1;

    return result;
}

Matrix Matrix::inverse_rotation_y(f64 angle)
{
    Matrix result;

    f64 sin_v = sin(angle * degrees_to_radians);
    f64 cos_v = cos(angle * degrees_to_radians);

    result.values[0][0] = cos_v;
    result.values[0][2] = -sin_v;
    result.values[1][1] = 1;
    result.values[2][0] = sin_v;
    result.values[2][2] = cos_v;
    result.values[3][3] = 1;

    return result;
}

Matrix Matrix::rotation_z(f64 angle)
{
    Matrix result;

    f64 sin_v = sin(angle * degrees_to_radians);
    f64 cos_v = cos(angle * degrees_to_radians);

    result.values[0][0] = cos_v;
    result.values[0][1] = -sin_v;
    result.values[1][0] = sin_v;
    result.values[1][1] = cos_v;
    result.values[2][2] = 1;
    result.values[3][3] = 1;

    return result;
}

Matrix Matrix::inverse_rotation_z(f64 angle)
{
    Matrix result;

    f64 sin_v = sin(angle * degrees_to_radians);
    f64 cos_v = cos(angle * degrees_to_radians);

    result.values[0][0] = cos_v;
    result.values[0][1] = sin_v;
    result.values[1][0] = -sin_v;
    result.values[1][1] = cos_v;
    result.values[2][2] = 1;
    result.values[3][3] = 1;

    return result;
}

Matrix Matrix::rotation(f64 angle_x, f64 angle_y, f64 angle_z)
{
    return rotation_y(angle_y) * rotation_x(angle_x) * rotation_z(angle_z);
}

Matrix Matrix::inverse_rotation(f64 angle_x, f64 angle_y, f64 angle_z)
{
    return inverse_rotation_z(angle_z) * inverse_rotation_x(angle_x) * inverse_rotation_y(angle_y);
}

Matrix Matrix::scale(f64 scale_x, f64 scale_y, f64 scale_z)
{
    $assert(scale_x != 0);
    $assert(scale_y != 0);
    $assert(scale_z != 0);

    Matrix result;

    result.values[0][0] = scale_x;
    result.values[1][1] = scale_y;
    result.values[2][2] = scale_z;
    result.values[3][3] = 1;

    return result;
}

Matrix Matrix::inverse_scale(f64 scale_x, f64 scale_y, f64 scale_z)
{
    $assert(scale_x != 0);
    $assert(scale_y != 0);
    $assert(scale_z != 0);

    Matrix result;

    result.values[0][0] = 1.0 / scale_x;
    result.values[1][1] = 1.0 / scale_y;
    result.values[2][2] = 1.0 / scale_z;
    result.values[3][3] = 1;

    return result;
}

Matrix Matrix::scale(f64 scale)
{
    $assert(scale != 0);

    Matrix result;

    for (i32 i = 0; i < 3; i++)
        result.values[i][i] = scale;
    result.values[3][3] = 1;

    return result;
}

Matrix Matrix::inverse_scale(f64 scale)
{
    $assert(scale != 0);

    Matrix result;

    for (i32 i = 0; i < 3; i++)
        result.values[i][i] = 1 / scale;
    result.values[3][3] = 1;

    return result;
}


Matrix Matrix::transpose() const
{
    Matrix result;

    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
        result.values[y][x] = this->values[x][y];

    return result;
}


bool Matrix::operator ==(const Matrix &other) const
{
    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
        if (this->values[y][x] != other.values[y][x])
            return false;
    return true;
}

bool Matrix::operator !=(const Matrix &other) const
{
    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
        if (this->values[y][x] != other.values[y][x])
            return true;
    return false;
}


Matrix Matrix::operator -() const
{
    Matrix result;

    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
        result.values[y][x] = -this->values[y][x];

    return result;
}


Point Matrix::operator *(const Point &point) const
{
    f64 result[3] = { 0, 0, 0 };

    for (i32 y = 0; y < 3; y++)
    for (i32 x = 0; x < 4; x++)
        result[y] += this->values[y][x] * point[x];
    
    return Point(result[0], result[1], result[2]);
}

Vector Matrix::operator *(const Vector &vector) const
{
    f64 result[3] = { 0, 0, 0 };

    for (i32 y = 0; y < 3; y++)
    for (i32 x = 0; x < 3; x++)
        result[y] += this->values[y][x] * vector[x];
    
    return Vector(result[0], result[1], result[2]);
}


Matrix Matrix::operator +(const Matrix &other) const
{
    Matrix result;

    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
        result.values[y][x] = this->values[y][x] + other.values[y][x];
    
    return result;
}

Matrix Matrix::operator -(const Matrix &other) const
{
    Matrix result;

    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
        result.values[y][x] = this->values[y][x] - other.values[y][x];
    
    return result;
}

Matrix Matrix::operator *(const Matrix &other) const
{
    Matrix result;

    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
    for (i32 i = 0; i < 4; i++)
        result.values[y][x] += this->values[y][i] * other.values[i][x];
    
    return result;
}

Matrix Matrix::operator *(f64 scalar) const
{
    Matrix result;

    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
        result.values[y][x] = this->values[y][x] * scalar;
    
    return result;
}

Matrix Matrix::operator /(f64 divisor) const
{
    $assert(divisor != 0);

    Matrix result;

    for (i32 y = 0; y < 4; y++)
    for (i32 x = 0; x < 4; x++)
        result.values[y][x] = this->values[y][x] / divisor;
    
    return result;
}
