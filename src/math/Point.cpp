
#include "Point.h"

#include "Vector.h"
#include <math.h>



Point::Point()
    : x(0), y(0), z(0)
{

}

Point::Point(f64 x, f64 y, f64 z)
    : x(x), y(y), z(z)
{

}


Vector Point::to_vector() const
{
    return Vector(x, y, z);
}

f64 Point::distance_to_squared(const Point &other) const
{
    f64 dx = this->x - other.x;
    f64 dy = this->y - other.y;
    f64 dz = this->z - other.z;
    return (dx * dx) + (dy * dy) + (dz * dz);
}

f64 Point::distance_to(const Point &other) const
{
    return sqrt(distance_to_squared(other));
}


f64 Point::operator [](i32 index) const
{
    $assert(index >= 0);
    $assert(index < 4);

    switch (index)
    {
        case 0: return x;
        case 1: return y;
        case 2: return z;
        case 3: return 1;
        default: $crash();
    }
}

bool Point::operator ==(const Point &other) const
{
    return (this->x == other.x) && (this->y == other.y) && (this->z == other.z);
}

bool Point::operator !=(const Point &other) const
{
    return (this->x != other.x) || (this->y != other.y) || (this->z != other.z);
}

Point Point::operator -() const
{
    return Point(-x, -y, -z);
}

Point Point::operator +(const Vector &vector) const
{
    return Point(
        this->x + vector.x, 
        this->y + vector.y, 
        this->z + vector.z
    );
}

Vector Point::operator -(const Point &other) const
{
    return Vector(
        this->x - other.x,
        this->y - other.y,
        this->z - other.z
    );
}

Point Point::operator *(f64 scalar) const
{
    return Point(
        this->x * scalar,
        this->y * scalar,
        this->z * scalar
    );
}

Point Point::operator /(f64 divisor) const
{
    $assert(divisor != 0);

    return Point(
        this->x / divisor,
        this->y / divisor,
        this->z / divisor
    );
}