
#ifndef RAY_TRACER_HEADER_POINT_H
#define RAY_TRACER_HEADER_POINT_H

#include "common.h"

class Vector;

class Point
{

    public:
        f64 x;
        f64 y;
        f64 z;

    public:
        Point();
        Point(f64 x, f64 y, f64 z);
        
        Point(const Point &other) = default;
        Point(Point &&) = default;
        ~Point() = default;
        Point &operator =(const Point &other) = default;
        Point &operator =(Point &&) = default;

    public:
        Vector to_vector() const;

        f64 distance_to_squared(const Point &other) const;
        f64 distance_to(const Point &other) const;

    public:
        f64 operator [](i32 index) const;

        bool operator ==(const Point &other) const;
        bool operator !=(const Point &other) const;

        Point operator -() const;

        Point operator +(const Vector &vector) const;
        Vector operator -(const Point &other) const;
        Point operator *(f64 scalar) const;
        inline friend Point operator *(f64 scalar, const Point &point) { return point * scalar; }
        Point operator /(f64 divisor) const;

};


#endif