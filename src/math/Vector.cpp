
#include "Vector.h"

#include "constants.h"
#include "Point.h"
#include <math.h>



Vector::Vector()
    : x(0), y(0), z(0)
{

}

Vector::Vector(f64 x, f64 y, f64 z)
    : x(x), y(y), z(z)
{

}


Point Vector::to_point() const
{
    return Point(x, y, z);
}

f64 Vector::length_squared() const
{
    return (x * x) + (y * y) + (z * z);
}

f64 Vector::length() const
{
    return sqrt(length_squared());
}

Vector Vector::normalize() const
{
    $assert(length_squared() != 0);

    return *this / length();
}

f64 Vector::dot(const Vector &other) const
{
    return (this->x * other.x) + (this->y * other.y) + (this->z * other.z);
}

Vector Vector::cross(const Vector &other) const
{
    return Vector(
        (this->y * other.z) - (this->z * other.y),
        (this->z * other.x) - (this->x * other.z),
        (this->x * other.y) - (this->y * other.x)
    );
}

f64 Vector::angle_to(const Vector &other) const
{
    return acos(this->normalize() * other.normalize()) * radians_to_degrees;
}


f64 Vector::operator [](i32 index) const
{
    $assert(index >= 0);
    $assert(index < 4);

    switch (index)
    {
        case 0: return x;
        case 1: return y;
        case 2: return z;
        case 3: return 0;
        default: $crash();
    }
}

bool Vector::operator ==(const Vector &other) const
{
    return (this->x == other.x) && (this->y == other.y) && (this->z == other.z);
}

bool Vector::operator !=(const Vector &other) const
{
    return (this->x != other.x) || (this->y != other.y) || (this->z != other.z);
}

Vector Vector::operator -() const
{
    return Vector(-x, -y, -z);
}

Vector Vector::operator +(const Vector &other) const
{
    return Vector(
        this->x + other.x,
        this->y + other.y,
        this->z + other.z
    );
}

Point Vector::operator +(const Point &point) const
{
    return Point(
        this->x + point.x,
        this->y + point.y,
        this->z + point.z
    );
}

Vector Vector::operator -(const Vector &other) const
{
    return Vector(
        this->x - other.x,
        this->y - other.y,
        this->z - other.z
    );
}

Point Vector::operator -(const Point &point) const
{
    return Point(
        this->x - point.x,
        this->y - point.y,
        this->z - point.z
    );
}

Vector Vector::operator *(f64 scalar) const
{
    return Vector(
        this->x * scalar,
        this->y * scalar,
        this->z * scalar
    );
}

Vector Vector::operator /(f64 divisor) const
{
    $assert(divisor != 0);

    return Vector(
        this->x / divisor,
        this->y / divisor,
        this->z / divisor
    );
}
