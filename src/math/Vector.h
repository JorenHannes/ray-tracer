
#ifndef RAY_TRACER_HEADER_VECTOR_H
#define RAY_TRACER_HEADER_VECTOR_H

#include "common.h"

class Point;

class Vector
{

    public:
        f64 x;
        f64 y;
        f64 z;

    public:
        Vector();
        Vector(f64 x, f64 y, f64 z);
        
        Vector(const Vector &other) = default;
        Vector(Vector &&) = default;
        ~Vector() = default;
        Vector &operator =(const Vector &other) = default;
        Vector &operator =(Vector &&) = default;

    public:
        Point to_point() const;

        f64 length_squared() const;
        f64 length() const;

        Vector normalize() const;

        f64 dot(const Vector &other) const;
        Vector cross(const Vector &other) const;

        f64 angle_to(const Vector &other) const;

    public:
        f64 operator [](i32 index) const;

        bool operator ==(const Vector &other) const;
        bool operator !=(const Vector &other) const;

        inline f64 operator *(const Vector &other) const { return dot(other); }

        Vector operator -() const;

        Vector operator +(const Vector &other) const;
        Point operator +(const Point &point) const;
        Vector operator -(const Vector &other) const;
        Point operator -(const Point &point) const;
        Vector operator *(f64 scalar) const;
        inline friend Vector operator *(f64 scalar, const Vector &vector) { return vector * scalar; }
        Vector operator /(f64 divisor) const;

};


#endif