
#ifndef RAY_TRACER_HEADER_MATRIX_H
#define RAY_TRACER_HEADER_MATRIX_H

#include "common.h"

#include "math/Point.h"
#include "math/Vector.h"


class Matrix
{

    private:
        f64 values[4][4];
    
    private:
        Matrix();
    
    public:
        Matrix(const Matrix &other) = default;
        Matrix(Matrix &&) = default;
        ~Matrix() = default;
        Matrix &operator =(const Matrix &other) = default;
        Matrix &operator =(Matrix &&) = default;

    public:
        static Matrix identity();

        static Matrix translation(f64 x, f64 y, f64 z);
        static Matrix inverse_translation(f64 x, f64 y, f64 z);
        
        static Matrix rotation_x(f64 angle);
        static Matrix inverse_rotation_x(f64 angle);
        static Matrix rotation_y(f64 angle);
        static Matrix inverse_rotation_y(f64 angle);
        static Matrix rotation_z(f64 angle);
        static Matrix inverse_rotation_z(f64 angle);
        static Matrix rotation(f64 angle_x, f64 angle_y, f64 angle_z);
        static Matrix inverse_rotation(f64 angle_x, f64 angle_y, f64 angle_z);

        static Matrix scale(f64 scale_x, f64 scale_y, f64 scale_z);
        static Matrix inverse_scale(f64 scale_x, f64 scale_y, f64 scale_z);
        static Matrix scale(f64 scale);
        static Matrix inverse_scale(f64 scale);

    public:
        Matrix transpose() const;

    public:
        bool operator ==(const Matrix &other) const;
        bool operator !=(const Matrix &other) const;

        Matrix operator -() const;

        Point operator *(const Point &point) const;
        Vector operator *(const Vector &vector) const;

        Matrix operator +(const Matrix &other) const;
        Matrix operator -(const Matrix &other) const;
        Matrix operator *(const Matrix &other) const;
        Matrix operator *(f64 scalar) const;
        inline friend Matrix operator *(f64 scalar, const Matrix &matrix) { return matrix * scalar; }
        Matrix operator /(f64 divisor) const;

};


#endif