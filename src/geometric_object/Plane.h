
#ifndef RAY_TRACER_HEADER_PLANE_H
#define RAY_TRACER_HEADER_PLANE_H

#include "common.h"
#include "I_GeometricObject.h"

#include "math/Point.h"
#include "math/Vector.h"
#include "util/Transform.h"
#include "shading/Material.h"
#include "ray/Ray.h"


class Plane : public I_GeometricObject
{

    public:
        Material material;
        Transform transform;

    public:
        Plane();
        Plane(f64 x, f64 y, f64 z, const Vector &normal, f64 kd, const Color &color);
        
        Plane(Plane &&) = default;
        ~Plane() = default;

        Plane(const Plane &other) = delete;
        Plane &operator =(const Plane &other) = delete;
        Plane &operator =(Plane &&) = delete;

    public:
        virtual const Material &get_material() const;

        virtual bool intersect(const Ray &ray, f64 &t, Point &hit_point, Vector &normal) const;
        virtual bool intersect_shadow(const Ray &ray, f64 &t) const;

};


#endif