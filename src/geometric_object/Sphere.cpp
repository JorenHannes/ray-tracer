
#include "Sphere.h"

#include "constants.h"
#include <math.h>



Sphere::Sphere()
    : transform(Transform::none()), material(Material { 1, Color(1, 0, 0) })
{
    
}

Sphere::Sphere(const Point &center, f64 radius, f64 kd, const Color &color)
    : transform(Transform::scale(radius) + Transform::translation(center.x, center.y, center.z)), material(Material { kd, color })
{
    $assert(radius > 0);
    $assert(kd >= 0);
    $assert(kd <= 1);
}


const Material &Sphere::get_material() const
{
    return material;
}


bool Sphere::intersect(const Ray &ray, f64 &t, Point &hit_point, Vector &normal) const
{
    f64 t_temp;
    if (!intersect_shadow(ray, t_temp) || t < t_temp)
        return false;

    t = t_temp;

    hit_point = ray.origin + (ray.direction * t);
    normal = transform.transform_normal(transform.inverse_transform(hit_point).to_vector()).normalize();

    return true;
}

bool Sphere::intersect_shadow(const Ray &ray, f64 &t) const
{
    // (o + dt - c)2 = r*r
    // (o + dt - c)2 - r*r = 0
    // (d*d)t2 + 2(o-c)*dt + (o-c)*(o-c) - (r*r) = 0

    Ray new_ray = transform.inverse_transform(ray);

    f64 a = new_ray.direction * new_ray.direction;
    f64 b = 2 * new_ray.origin.to_vector() * new_ray.direction;
    f64 c = new_ray.origin.to_vector().length_squared() - 1;

    f64 disc = (b * b) - (4 * a * c);

    if (disc < 0)
        return false;

    if (disc == 0)
    {
        t = -b / (2 * a);
    }
    else
    {
        f64 t1 = (-b + sqrt(disc)) / (2 * a);
        f64 t2 = (-b - sqrt(disc)) / (2 * a);

        if (t1 > k_epsilon && t1 < t2)
            t = t1;
        else if (t2 > k_epsilon)
            t = t2;
        else
            return false;
    }

    return true;
}