
#ifndef RAY_TRACER_HEADER_SPHERE_H
#define RAY_TRACER_HEADER_SPHERE_H

#include "common.h"
#include "I_GeometricObject.h"

#include "math/Point.h"
#include "math/Vector.h"
#include "ray/Ray.h"
#include "util/Transform.h"
#include "shading/Material.h"


class Sphere : public I_GeometricObject
{

    public:
        Material material;
        Transform transform;

    public:
        Sphere();
        Sphere(const Point &center, f64 radius, f64 kd, const Color &color);
        
        Sphere(Sphere &&) = default;
        ~Sphere() = default;
        Sphere &operator =(Sphere &&) = default;

        Sphere(const Sphere &other) = delete;
        Sphere &operator =(const Sphere &other) = delete;

    public:
        virtual const Material &get_material() const;

        virtual bool intersect(const Ray &ray, f64 &t, Point &hit_point, Vector &normal) const;
        virtual bool intersect_shadow(const Ray &ray, f64 &t) const;

};


#endif