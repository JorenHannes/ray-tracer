
#include "Plane.h"

#include "constants.h"



Plane::Plane()
    : transform(Transform::none()), material(Material { 1, Color(0, 0, 1) })
{

}

Plane::Plane(f64 x, f64 y, f64 z, const Vector &normal, f64 kd, const Color &color)
    : transform(Transform::none()), material(Material { kd, color })
{
    $assert(kd >= 0);
    $assert(kd <= 1);

    Vector up = Vector(0, 1, 0);
    Vector xy = Vector(normal.x, normal.y, 0);
    Vector yz = Vector(0, normal.y, normal.z);

    f64 sign_x = (normal.x < 0 ? -1 : 1);
    f64 sign_z = (normal.z < 0 ? -1 : 1);

    transform = Transform::rotation(sign_z * up.angle_to(yz), 0, sign_x * up.angle_to(xy)) + Transform::translation(x, y, z);
}


const Material &Plane::get_material() const
{
    return material;
}


bool Plane::intersect(const Ray &ray, f64 &t, Point &hit_point, Vector &normal) const
{
    // (o + dt - p) * normal = 0
    // t(d * normal) + ((o - p) * normal) = 0
    // t = -((o - p) * normal) / (d * normal)

    f64 t_temp;
    if (!intersect_shadow(ray, t_temp) || t < t_temp)
        return false;

    t = t_temp;

    hit_point = ray.origin + (ray.direction * t);
    normal = transform.transform_normal(Vector(0, 1, 0));

    return true;
}

bool Plane::intersect_shadow(const Ray &ray, f64 &t) const
{
    Ray new_ray = transform.inverse_transform(ray);

    if (new_ray.direction.y == 0)
        return false;

    t = -new_ray.origin.y / new_ray.direction.y;

    return t >= k_epsilon;
}
