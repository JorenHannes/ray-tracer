
#ifndef RAY_TRACER_HEADER_I_GEOMETRIC_OBJECT_H
#define RAY_TRACER_HEADER_I_GEOMETRIC_OBJECT_H

#include "common.h"

#include "ray/Ray.h"
#include "math/Point.h"
#include "math/Vector.h"
#include "shading/Material.h"


class I_GeometricObject
{
    public:
        virtual const Material &get_material() const = 0;

        virtual bool intersect(const Ray &ray, f64 &t, Point &hit_point, Vector &normal) const = 0;
        virtual bool intersect_shadow(const Ray &ray, f64 &t) const = 0;

};


#endif