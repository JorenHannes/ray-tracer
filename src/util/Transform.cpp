
#include "Transform.h"



Transform::Transform(const Matrix &transform, const Matrix &inverse_transform)
    : matrix(transform), inverse_matrix(inverse_transform)
{

}


Transform Transform::none()
{
    return Transform(Matrix::identity(), Matrix::identity());
}

Transform Transform::translation(f64 x, f64 y, f64 z)
{
    return Transform(Matrix::translation(x, y, z), Matrix::inverse_translation(x, y, z));
}

Transform Transform::rotation_x(f64 angle)
{
    return Transform(Matrix::rotation_x(angle), Matrix::inverse_rotation_x(angle));
}

Transform Transform::rotation_y(f64 angle)
{
    return Transform(Matrix::rotation_y(angle), Matrix::inverse_rotation_y(angle));
}

Transform Transform::rotation_z(f64 angle)
{
    return Transform(Matrix::rotation_z(angle), Matrix::inverse_rotation_z(angle));
}

Transform Transform::rotation(f64 angle_x, f64 angle_y, f64 angle_z)
{
    return Transform(Matrix::rotation(angle_x, angle_y, angle_z), Matrix::inverse_rotation(angle_x, angle_y, angle_z));
}

Transform Transform::scale(f64 scale_x, f64 scale_y, f64 scale_z)
{
    return Transform(Matrix::scale(scale_x, scale_y, scale_z), Matrix::inverse_scale(scale_x, scale_y, scale_z));
}

Transform Transform::scale(f64 scale)
{
    return Transform(Matrix::scale(scale), Matrix::inverse_scale(scale));
}


Point Transform::transform(const Point &point) const
{
    return matrix * point;
}

Point Transform::inverse_transform(const Point &point) const
{
    return inverse_matrix * point;
}


Vector Transform::transform(const Vector &vector) const
{
    return matrix * vector;
}

Vector Transform::inverse_transform(const Vector &vector) const
{
    return inverse_matrix * vector;
}


Vector Transform::transform_normal(const Vector &normal) const
{
    return inverse_matrix.transpose() * normal;
}

Vector Transform::inverse_transform_normal(const Vector &normal) const
{
    return matrix.transpose() * normal;
}


Ray Transform::transform(const Ray &ray) const
{
    return Ray(
        matrix * ray.origin,
        matrix * ray.direction
    );
}

Ray Transform::inverse_transform(const Ray &ray) const
{
    return Ray(
        inverse_matrix * ray.origin,
        inverse_matrix * ray.direction
    );
}


bool Transform::operator ==(const Transform &other) const
{
    return (this->matrix == other.matrix);
}

bool Transform::operator !=(const Transform &other) const
{
    return (this->matrix != other.matrix);
}


Transform Transform::operator +(const Transform &other) const
{
    return Transform(
        other.matrix * this->matrix,
        this->inverse_matrix * other.inverse_matrix
    );
}

Transform &Transform::operator +=(const Transform &other)
{
    this->matrix = other.matrix * this->matrix;
    this->inverse_matrix = this->inverse_matrix * other.inverse_matrix;
    return *this;
}
