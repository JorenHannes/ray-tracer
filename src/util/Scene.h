
#ifndef RAY_TRACER_HEADER_SCENE_H
#define RAY_TRACER_HEADER_SCENE_H

#include "common.h"

#include "camera/PerspectiveCamera.h"
#include "geometric_object/I_GeometricObject.h"
#include "light/PointLight.h"
#include "sample/Sample.h"
#include "math/Point.h"
#include <vector>


struct Scene
{

    PerspectiveCamera camera;
    std::vector<I_GeometricObject *> geometric_objects;
    std::vector<PointLight> lights;
    u32 samples_per_pixel;
    void (*sampler)(i32, i32, u32, Sample *);

};


#endif