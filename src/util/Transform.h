
#ifndef RAY_TRACER_HEADER_TRANSFORM_H
#define RAY_TRACER_HEADER_TRANSFORM_H

#include "common.h"

#include "math/Matrix.h"
#include "math/Point.h"
#include "math/Vector.h"
#include "ray/Ray.h"


class Transform
{

    private:
        Matrix matrix;
        Matrix inverse_matrix;

    private:
        Transform(const Matrix &transform, const Matrix &inverse_transform);
    
    public:
        Transform(const Transform &other) = default;
        Transform(Transform &&) = default;
        ~Transform() = default;
        Transform &operator =(const Transform &other) = default;
        Transform &operator =(Transform &&) = default;

        Transform() = delete;

    public:
        static Transform none();
        static Transform translation(f64 x, f64 y, f64 z);
        static Transform rotation_x(f64 angle);
        static Transform rotation_y(f64 angle);
        static Transform rotation_z(f64 angle);
        static Transform rotation(f64 angle_x, f64 angle_y, f64 angle_z);
        static Transform scale(f64 scale_x, f64 scale_y, f64 scale_z);
        static Transform scale(f64 scale);
    
    public:
        Point transform(const Point &point) const;
        Point inverse_transform(const Point &point) const;

        Vector transform(const Vector &vector) const;
        Vector inverse_transform(const Vector &vector) const;

        Vector transform_normal(const Vector &normal) const;
        Vector inverse_transform_normal(const Vector &normal) const;

        Ray transform(const Ray &ray) const;
        Ray inverse_transform(const Ray &ray) const;

    public:
        bool operator ==(const Transform &other) const;
        bool operator !=(const Transform &other) const;

        Transform operator +(const Transform &other) const;
        Transform &operator +=(const Transform &other);

};


#endif