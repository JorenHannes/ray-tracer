
#include "Timer.h"

#include "windows.h"



Timer::Timer()
    : last_counter(0)
{
    reset();
}


void Timer::reset()
{
    LARGE_INTEGER time;
    if (!QueryPerformanceCounter(&time))
        $crash();
    
    last_counter = time.QuadPart;
}

f64 Timer::get_elapsed_time() const
{
    LARGE_INTEGER time;
    if (!QueryPerformanceCounter(&time))
        $crash();
    
    LARGE_INTEGER frequency;
    if (!QueryPerformanceFrequency(&frequency))
        $crash();
    
    f64 passed_counter = time.QuadPart - last_counter;

    return passed_counter / frequency.QuadPart;
}
