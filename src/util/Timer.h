
#ifndef RAY_TRACER_HEADER_TIMER_H
#define RAY_TRACER_HEADER_TIMER_H

#include "common.h"


class Timer
{

    private:
        f64 last_counter;

    public:
        Timer();
        Timer(const Timer &other) = default;
        Timer(Timer &&) = default;
        ~Timer() = default;
        Timer &operator =(const Timer &other) = default;
        Timer &operator =(Timer &&) = default;

    public:
        void reset();
        f64 get_elapsed_time() const;

};


#endif