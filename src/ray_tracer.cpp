
#include "common.h"

#include "constants.h"
#include "util/Scene.h"
#include "window/FrameBuffer.h"
#include "threading/ThreadService.h"
#include "threading/Tile.h"
#include "camera/PerspectiveCamera.h"
#include "geometric_object/I_GeometricObject.h"
#include "geometric_object/Sphere.h"
#include "geometric_object/Plane.h"
#include "light/PointLight.h"
#include "util/Timer.h"
#include "sample/Sample.h"
#include "sample/Sampler.h"
#include <stdio.h>
#include <float.h>



static Scene scene;


void init_scene(FrameBuffer &buffer)
{
    
    scene.camera = PerspectiveCamera(0, 0, 0, buffer.width, buffer.height, 67);

    Sphere sphere = Sphere(Point(0, 0, -6), 4, 1, Color(1, 0, 0));
    Plane plane = Plane(0, -4, 0, Vector(0, 1, 0), 1, Color(0, 0, 1));

    scene.geometric_objects.push_back(&sphere);
    scene.geometric_objects.push_back(&plane);

    scene.lights.push_back(PointLight(0, 4, 0, 2000, Color(1, 1, 1)));

    scene.samples_per_pixel = 16;
    scene.sampler = &sample_jittered;

    Timer timer;

    ThreadService service(buffer);
    service.start();

    printf("Render Time: %fs\n", timer.get_elapsed_time());

}

void render_tile(Tile &tile)
{
    for (i32 y = 0; y < tile.height; y++)
    for (i32 x = 0; x < tile.width; x++)
    {
        Sample samples[scene.samples_per_pixel];
        scene.sampler(tile.x + x, tile.y + y, scene.samples_per_pixel, samples);
        for (i32 i = 0; i < scene.samples_per_pixel; i++)
        {

            Ray ray = scene.camera.generate_ray(samples[i]);

            Color color = Color();
            f64 t = DBL_MAX;
            Point hit_point;
            Vector normal;

            I_GeometricObject *hit_object;
            for (I_GeometricObject *geometric_object : scene.geometric_objects)
                if (geometric_object->intersect(ray, t, hit_point,  normal))
                    hit_object = geometric_object;

            for (PointLight light : scene.lights)
            {

                Vector l = (light.position - hit_point).normalize();
                f64 cos = (l * normal);
                if (cos <= 0)
                    continue;

                Ray shadow_ray = Ray(hit_point, l);
                bool shadow_hit = false;
                for (I_GeometricObject *other_object : scene.geometric_objects)
                {
                    if (other_object == hit_object)
                        continue;

                    f64 t;
                    if (!(other_object->intersect_shadow(shadow_ray, t) && t > k_epsilon && (t * t) < light.position.distance_to_squared(hit_point)))
                        continue;

                    shadow_hit = true;
                    break;
                }

                if (!shadow_hit)
                    color = color + (hit_object->get_material().color * light.color * (((hit_object->get_material().kd * light.get_L(hit_point)) / pi) * cos));

            }

            tile.get_pixel(x, y) = color.normalize();

        }

    }
}
