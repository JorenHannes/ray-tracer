
#include "ThreadService.h"

#include <windows.h>



void render_tile(Tile &tile);


internal DWORD thread_func(LPVOID lpParam)
{
    ThreadService *service = (ThreadService *)lpParam;

    while (true)
    {
        service->lock();
        if (!service->has_tile())
            break;
        Tile &tile = service->get_tile();
        service->unlock();

        render_tile(tile);
    }
    service->unlock();

    return 0;
}



ThreadService::ThreadService(FrameBuffer &buffer)
{
     i32 tiles_x = buffer.width / 64;
    if (buffer.width % 64)
        tiles_x++;

    i32 tiles_y = buffer.height / 64;
    if (buffer.height % 64)
        tiles_y++;

    tiles = new Tile[tiles_x * tiles_y];
    for (i32 y = 0; y < tiles_y; y++)
    for (i32 x = 0; x < tiles_x; x++)
    {
        i32 width = ((x * 64) + 64 >= buffer.width ? buffer.width - (x * 64) : 64);
        i32 height = ((y * 64) + 64 >= buffer.height ? buffer.height - (y * 64) : 64);
        tiles[x + (y * tiles_x)] = Tile(x * 64, y * 64, width, height, buffer);
    }

    mutex = CreateMutex(NULL, false, NULL);
    next_index = (tiles_x * tiles_y) - 1;
}

ThreadService::~ThreadService()
{
    delete tiles;
    CloseHandle(mutex);
}


void ThreadService::start()
{
    SYSTEM_INFO sys_info;
    GetSystemInfo(&sys_info);
    i32 number_of_threads = sys_info.dwNumberOfProcessors;

    HANDLE threads[number_of_threads];
    for (i32 i = 0; i < number_of_threads; i++)
        threads[i] = CreateThread(NULL, 0, thread_func, (void *)this, 0, NULL);
    
    WaitForMultipleObjects(number_of_threads, threads, true, INFINITE);

    for (i32 i = 0; i < number_of_threads; i++)
        CloseHandle(threads[i]);
}


void ThreadService::lock()
{
    WaitForSingleObject(mutex, INFINITE);
    $assert(!locked);
    locked = true;
}

void ThreadService::unlock()
{
    if (!locked)
        return;

    ReleaseMutex(mutex);
    locked = false;
}


bool ThreadService::has_tile() const
{
    return next_index > -1;
}

Tile &ThreadService::get_tile()
{
    return tiles[next_index--];
}
