
#ifndef RAY_TRACER_HEADER_THREADSERVICE_H
#define RAY_TRACER_HEADER_THREADSERVICE_H

#include "common.h"

#include "Tile.h"
#include "window/FrameBuffer.h"


class ThreadService
{

    private:
        Tile *tiles;
        void* mutex;
        i32 next_index;
        bool locked;

    public:
        ThreadService(FrameBuffer &buffer);
        ~ThreadService();

        ThreadService() = delete;
        ThreadService(const ThreadService &other) = delete;
        ThreadService &operator =(const ThreadService &other) = delete;

    public:
        void start();

        void lock();
        void unlock();

        bool has_tile() const;
        Tile &get_tile();

};


#endif