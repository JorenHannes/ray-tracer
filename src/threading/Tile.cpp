
#include "Tile.h"



Tile::Tile(i32 x, i32 y, i32 width, i32 height, FrameBuffer &buffer)
    : x(x), y(y), width(width), height(height), buffer(&buffer)
{

}

Tile::Tile()
    : x(0), y(0), width(0), height(0), buffer(0)
{

}

Tile::Tile(const Tile &other)
    : x(other.x), y(other.y), width(other.width), height(other.height), buffer(other.buffer)
{

}

Tile &Tile::operator =(const Tile &other)
{
    this->x = other.x;
    this->y = other.y;
    this->width = other.width;
    this->height = other.height;
    this->buffer = other.buffer;
}


Pixel &Tile::get_pixel(i32 x, i32 y)
{
    $assert(x >= 0);
    $assert(x < this->width);
    $assert(y >= 0);
    $assert(y < this->height);

    return buffer->get_pixel(this->x + x, this->y + y);
}
