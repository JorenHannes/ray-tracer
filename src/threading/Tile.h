
#ifndef RAY_TRACER_HEADER_TILE_H
#define RAY_TRACER_HEADER_TILE_H

#include "common.h"

#include "window/FrameBuffer.h"
#include "window/Pixel.h"


class Tile
{

    public:
        i32 x;
        i32 y;
        i32 width;
        i32 height;
    
    private:
        FrameBuffer *buffer;

    public:
        Tile(i32 x, i32 y, i32 width, i32 height, FrameBuffer &buffer);
        Tile();
        ~Tile() = default;
        Tile(const Tile &other);
        Tile &operator =(const Tile &other);

    public:
        Pixel &get_pixel(i32 x, i32 y);

};


#endif