
#ifndef RAY_TRACER_HEADER_RAY_H
#define RAY_TRACER_HEADER_RAY_H

#include "common.h"

#include "math/Point.h"
#include "math/Vector.h"


class Ray
{

    public:
        const Point origin;
        const Vector direction;

    public:
        Ray();
        Ray(const Point &origin, const Vector &direction);
        
        Ray(const Ray &ray) = default;
        Ray(Ray &&) = default;
        ~Ray() = default;
        Ray &operator =(const Ray &ray) = default;
        Ray &operator =(Ray &&) = default;

};


#endif